<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('customers', function (Blueprint $table) {
            $table->id();
            $table->string('status');
            $table->string('first_name');
            $table->string('last_name');
            $table->string('email');
            $table->string('email_confirm')->nullable();
            $table->string('phone')->nullable();
            $table->string('phone_confirm')->nullable();
            $table->text('address');
            $table->timestamp('latest_activists_at')->nullable();
            $table->timestamp('deleted_at')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('customers');
    }
};
