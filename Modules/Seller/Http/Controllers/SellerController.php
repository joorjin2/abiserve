<?php

namespace Modules\Seller\Http\Controllers;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;

class SellerController extends Controller
{
    public function dashboard(Request $request){


        return view('seller::dashboard');
    }
}
