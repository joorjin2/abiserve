<?php

interface Irepository{
    public static function all();
    public static function first();
    public static function create();
}
